import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ResepComponent } from './pages/resep/resep.component';
import { KirimResepComponent } from './pages/kirim-resep/kirim-resep.component';
import { KhasComponent } from './pages/khas/khas.component';
import { KetanDurianComponent } from './pages/ketan-durian/ketan-durian.component';
import { AyamPopComponent } from './pages/ayam-pop/ayam-pop.component';
import { SotoComponent } from './pages/soto/soto.component';
import { NusantaraComponent } from './pages/nusantara/nusantara.component';
import { NuggetComponent } from './pages/nugget/nugget.component';
import { LontongComponent } from './pages/lontong/lontong.component';
import { SotoBComponent } from './pages/soto-b/soto-b.component';
import { DabuDabuComponent } from './pages/dabu-dabu/dabu-dabu.component';
import { SambalComponent } from './pages/sambal/sambal.component';
import { ManggaMudaComponent } from './pages/mangga-muda/mangga-muda.component';
import { PetaiComponent } from './pages/petai/petai.component';
import { KONTAKComponent } from './pages/kontak/kontak.component';

const routes: Routes = [
{
path:'home',
component:HomeComponent
},
{
path:'resep',
component:ResepComponent
},
{
path:'kirimresep',
component:KirimResepComponent
},
{
path:'khas',
component:KhasComponent
},
{
path:'ketandurian',
component:KetanDurianComponent
},
{
path:'ayampop',
component:AyamPopComponent
},
{
path:'soto',
component:SotoComponent
},
{
path:'nusantara',
component:NusantaraComponent
},
{
path:'nugget',
component:NuggetComponent
},
{
path:'lontong',
component:LontongComponent
},
{
path:'sotob',
component:SotoBComponent
},
{
path:'dabudabu',
component:DabuDabuComponent
},
{
path:'sambal',
component:SambalComponent
},
{
path:'manggamuda',
component:ManggaMudaComponent
},
{
path:'petai',
component:PetaiComponent
},
{
path:'kontak',
component:KONTAKComponent
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
