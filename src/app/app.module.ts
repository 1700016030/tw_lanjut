import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { ResepComponent } from './pages/resep/resep.component';
import { KirimResepComponent } from './pages/kirim-resep/kirim-resep.component';
import { KhasComponent } from './pages/khas/khas.component';
import { KetanDurianComponent } from './pages/ketan-durian/ketan-durian.component';
import { AyamPopComponent } from './pages/ayam-pop/ayam-pop.component';
import { SotoComponent } from './pages/soto/soto.component';
import { NusantaraComponent } from './pages/nusantara/nusantara.component';
import { NuggetComponent } from './pages/nugget/nugget.component';
import { LontongComponent } from './pages/lontong/lontong.component';
import { SotoBComponent } from './pages/soto-b/soto-b.component';
import { DabuDabuComponent } from './pages/dabu-dabu/dabu-dabu.component';
import { SambalComponent } from './pages/sambal/sambal.component';
import { ManggaMudaComponent } from './pages/mangga-muda/mangga-muda.component';
import { PetaiComponent } from './pages/petai/petai.component';
import { KONTAKComponent } from './pages/kontak/kontak.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ResepComponent,
    KirimResepComponent,
	KhasComponent,
	KetanDurianComponent,
	AyamPopComponent,
	SotoComponent,
	NusantaraComponent,
	NuggetComponent,
	LontongComponent,
	SotoBComponent,
	DabuDabuComponent,
	SambalComponent,
	ManggaMudaComponent,
	PetaiComponent,
	KONTAKComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
