import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManggaMudaComponent } from './mangga-muda.component';

describe('ManggaMudaComponent', () => {
  let component: ManggaMudaComponent;
  let fixture: ComponentFixture<ManggaMudaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManggaMudaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManggaMudaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
