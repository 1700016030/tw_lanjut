import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AyamPopComponent } from './ayam-pop.component';

describe('AyamPopComponent', () => {
  let component: AyamPopComponent;
  let fixture: ComponentFixture<AyamPopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AyamPopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AyamPopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
