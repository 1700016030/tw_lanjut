import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KhasComponent } from './khas.component';

describe('KhasComponent', () => {
  let component: KhasComponent;
  let fixture: ComponentFixture<KhasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KhasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KhasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
